#include <typi/type.hpp>

namespace typi::type_tree
{

	namespace
	{

		struct visitor : type::const_visitor<type, visitor>
		{
			std::map<variable, type> const & m;

			visitor (std::map<variable, type> const & m)
				: m{m}
			{ }

			type operator() (bool_type b)
			{
				return b;
			}

			type operator() (int_type i)
			{
				return i;
			}

			type operator() (variable const & v)
			{
				if (m.count(v) > 0)
					return m.at(v);
				else
					return v;
			}

			type operator() (function const & f)
			{
				return function{ visit(f.arg), visit(f.ret) };
			}

			type operator() (product const & p)
			{
				product res;
				for (auto const & t : p)
					res.push_back(visit(t));
				return res;
			}
		};

	}

	type substitute (type const & t, std::map<variable, type> const & m)
	{
		return visitor{m}.visit(t);
	}

}
