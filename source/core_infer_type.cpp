#include <typi/core.hpp>
#include <typi/utility.hpp>

namespace typi
{

	namespace
	{

		using result_t = std::pair<type_tree::type, type_map_t>;

		struct visitor : expr_tree::expression::const_visitor<result_t, visitor>
		{
			type_tree::variable cur_var;

			visitor ( )
				: cur_var{ "a" }
			{ }

			type_tree::variable new_var ( )
			{
				auto v = cur_var;
				cur_var = next_var(cur_var);
				return v;
			}

			result_t operator() (expr_tree::bool_constant)
			{
				return {type_tree::bool_type{}, {}};
			}

			result_t operator() (expr_tree::int_constant)
			{
				return {type_tree::int_type{}, {}};
			}

			result_t operator() (expr_tree::variable const & v)
			{
				auto t = new_var();

				return {t, {{v, t}}};
			}

			result_t operator() (expr_tree::foreign const & f)
			{
				auto t = f.type;

				// rename vars in 't'
				std::map<type_tree::variable, type_tree::type> rename_map;

				auto t_fv = free_vars(t);

				for (auto const & v : t_fv)
					rename_map[v] = new_var();

				t = substitute(t, rename_map);

				for (std::size_t i = 0; i < f.args.size(); ++i)
					t = ret_type(t);

				return {f.type, {}};
			}

			result_t operator() (expr_tree::abstraction const & ab)
			{
				auto res = visit(ab.body);

				std::vector<type_tree::type> var_types(ab.vars.size());

				for (std::size_t i = 0; i < ab.vars.size(); ++i)
				{
					var_types[i] = (res.second.count(ab.vars[i]) > 0) ? res.second[ab.vars[i]] : new_var();
					res.second.erase(ab.vars[i]);
				}

				if (ab.vars.size() == 1)
					return {type_tree::function{ var_types[0], res.first }, res.second};
				else
					return {type_tree::function{ var_types, res.first }, res.second};
			}

			result_t operator() (expr_tree::application const & app)
			{
				auto func = visit(app.func);
				auto arg = visit(app.arg);

				// 'func' == 'arg -> xxx' ?

				auto ret = new_var();

				auto func_expected = type_tree::function{ arg.first, ret };

				std::vector<std::pair<type_tree::type, type_tree::type>> equations;

				equations.push_back({func.first, func_expected});

				for (auto const & p : func.second)
				{
					if (arg.second.count(p.first) > 0)
					{
						equations.push_back({p.second, arg.second[p.first]});
					}
				}

				auto m = func.second;
				m.insert(arg.second.begin(), arg.second.end());

				return apply_unifier(ret, equations, m);
			}

			result_t operator() (expr_tree::product const & p)
			{
				std::vector<result_t> results;

				for (auto const & t : p)
					results.push_back(visit(t));

				std::vector<std::pair<type_tree::type, type_tree::type>> equations;
				type_map_t m;
				type_tree::product ret;

				for (std::size_t i = 0; i < results.size(); ++i)
				{
					m.insert(results[i].second.begin(), results[i].second.end());
					ret.push_back(results[i].first);

					for (std::size_t j = i + 1; j < results.size(); ++j)
					{
						for (auto const & p : results[j].second)
						{
							if (results[i].second.count(p.first) > 0)
							{
								equations.push_back({p.second, results[i].second[p.first]});
							}
						}
					}
				}

				return apply_unifier(ret, equations, m);
			}

			result_t apply_unifier (type_tree::type ret, std::vector<std::pair<type_tree::type, type_tree::type>> const & equations, type_map_t m)
			{
				auto unifier = unroll(unify(equations));

				for (auto & p : m)
				{
					p.second = substitute(p.second, unifier);
				}

				return {substitute(ret, unifier), std::move(m)};
			}
		};

	}

	std::pair<type_tree::type, type_map_t> infer_type (expr_tree::expression const & e)
	{
		return visitor{}.visit(e);
	}


}
