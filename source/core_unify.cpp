#include <typi/core.hpp>

#include <sstream>

namespace typi
{

	unifier_t unify (std::vector<equation_t> equations)
	{
		unifier_t u;

		while (!equations.empty())
		{
			auto eq = std::move(equations.back());
			equations.pop_back();

			// delete
			if (eq.first == eq.second)
				continue;

			// decompose function
			{
				auto f1 = std::get_if<type_tree::function>(eq.first.ptr.get());
				auto f2 = std::get_if<type_tree::function>(eq.second.ptr.get());

				if (f1 && f2)
				{
					equations.push_back({f1->arg, f2->arg});
					equations.push_back({f1->ret, f2->ret});
					continue;
				}
			}

			// decompose product
			{
				auto p1 = std::get_if<type_tree::product>(eq.first.ptr.get());
				auto p2 = std::get_if<type_tree::product>(eq.second.ptr.get());

				if (p1 && p2 && (p1->size() == p2->size()))
				{
					for (std::size_t i = 0; i < p1->size(); ++i)
					{
						equations.push_back({(*p1)[i], (*p2)[i]});
					}
					continue;
				}
			}

			{
				auto v1 = std::get_if<type_tree::variable>(eq.first.ptr.get());

				if (v1)
				{
					auto fv = free_vars(eq.second);

					if (fv.count(*v1) > 0)
					{
						// check
						std::ostringstream oss;
						oss << "Attempt to construct an infinite type " << *v1 << " = " << eq.second;
						throw std::runtime_error(oss.str());
					}
					else
					{
						// eliminate
						u[*v1] = eq.second;

						std::map<type_tree::variable, type_tree::type> m;
						m[*v1] = eq.second;

						for (auto & p : equations)
						{
							p.first = substitute(p.first, m);
							p.second = substitute(p.second, m);
						}

						continue;
					}
				}
			}

			// swap
			{
				auto v2 = std::get_if<type_tree::variable>(eq.second.ptr.get());

				if (v2)
				{
					equations.push_back({eq.second, eq.first});
					continue;
				}
			}

			std::ostringstream oss;
			oss << "Failed to unify " << eq.first << " with " << eq.second;
			throw std::runtime_error(oss.str());
		}

		return u;
	}

	unifier_t unroll (unifier_t u)
	{
		while (true)
		{
			std::map<type_tree::variable, type_tree::type> new_u;

			for (auto const & p : u)
				new_u[p.first] = substitute(p.second, u);

			if (new_u == u)
				return new_u;

			std::swap(u, new_u);
		}
	}

}
