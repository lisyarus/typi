#include <typi/type.hpp>

namespace typi::type_tree
{

	namespace
	{

		using var_set = std::set<type_tree::variable>;

		struct visitor : type::const_visitor<var_set, visitor>
		{
			var_set operator() (type_tree::variable const & v)
			{
				return {v};
			}

			var_set operator() (type_tree::bool_type)
			{
				return {};
			}

			var_set operator() (type_tree::int_type)
			{
				return {};
			}

			var_set operator() (type_tree::function const & f)
			{
				auto s1 = visit(f.arg);
				auto s2 = visit(f.ret);

				s1.insert(s2.begin(), s2.end());
				return s1;
			}

			var_set operator() (type_tree::product const & p)
			{
				var_set res;

				for (auto const & t : p)
				{
					auto s = visit(t);
					res.insert(s.begin(), s.end());
				}

				return res;
			}
		};

	}

	std::set<type_tree::variable> free_vars (type_tree::type const & t)
	{
		return visitor{}.visit(t);
	}

}

