#include <typi/parser.hpp>

#include <algorithm>
#include <map>

namespace typi
{

	expr_tree::expression fill_keywords (expr_tree::expression const & e)
	{
		static const std::map<expr_tree::variable, expr_tree::expression> keywords =
		{
			{ "true", true },
			{ "false", false },
			{ "unit", expr_tree::product{} },
		};

		return expr_tree::substitute(e, keywords);
	}

}
