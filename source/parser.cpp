#include <typi/parser.hpp>

#include <sstream>
#include <stdexcept>

namespace typi
{

	static std::set<char> special_symbols =
	{
		'(',
		')',
		'\\',
		',',
	};

	static bool is_literal (char c)
	{
		return !std::isspace(c) && (special_symbols.count(c) == 0);
	}

	expr_tree::expression parser::parse ( )
	{
		skip_space();
		return fill_keywords(parse_application());
	}

	void parser::check_eof ( ) const
	{
		if (eof())
			throw std::runtime_error("unexpected end");
	}

	void parser::skip_space ( )
	{
		while (!eof() && std::isspace(peek()))
			next();
	}

	void parser::expect (char c)
	{
		if (peek() != c)
			throw std::runtime_error("expected '" + std::string{c} + "'");
		next();
	}

	expr_tree::variable parser::parse_name ( )
	{
		std::ostringstream oss;
		while (!eof() && is_literal(peek()))
			oss << next();
		return oss.str();
	}

	expr_tree::int_constant parser::parse_int ( )
	{
		expr_tree::int_constant i = 0;
		while (!eof() && std::isdigit(peek()))
			i = i * 10 + (next() - '0');
		return i;
	}

	expr_tree::expression parser::parse_literal ( )
	{
		check_eof();

		if (std::isdigit(peek()))
			return parse_int();
		else
			return parse_name();
	}

	expr_tree::expression parser::parse_term ( )
	{
		if (peek() == '(')
		{
			next();
			skip_space();
			auto t = parse_application();
			if (peek() == ')')
			{
				next();
				skip_space();
				return t;
			}
			else if (peek() == ',')
			{
				expr_tree::product p;
				p.push_back(t);
				while (peek() == ',')
				{
					next();
					skip_space();
					p.push_back(parse_application());
				}

				skip_space();
				expect(')');
				skip_space();
				return p;
			}
			else
			{
				throw std::runtime_error("expected ')' or ','");
			}
		}
		else if (peek() == '\\')
		{
			next();
			skip_space();
			std::vector<expr_tree::variable> vars;
			vars.push_back(parse_name());
			skip_space();
			while (peek() != '-')
			{
				expect(',');
				skip_space();
				vars.push_back(parse_name());
				skip_space();
			}
			expect('-');
			expect('>');
			skip_space();
			return expr_tree::abstraction{vars, parse_application()};
		}
		else
		{
			auto value = parse_literal();
			skip_space();
			return value;
		}
	}

	expr_tree::expression parser::parse_application ( )
	{
		expr_tree::expression result = parse_term();

		while (!eof() && (is_literal(peek()) || (peek() == '(') || (peek() == '\\')))
		{
			result = expr_tree::application{result, parse_term()};
		}

		return result;
	}

}
