#include <typi/expression.hpp>

namespace typi::expr_tree
{

	namespace
	{

		struct visitor : expression::const_visitor<void, visitor>
		{
			std::ostream & out;

			visitor (std::ostream & out)
				: out{out}
			{ }

			void operator() (variable const & v)
			{
				out << v;
			}

			void operator() (bool_constant const & b)
			{
				out << (b ? "true" : "false");
			}

			void operator() (int_constant const & i)
			{
				out << i;
			}

			void operator() (foreign const & f)
			{
				if (f.args.empty())
					out << f.name;
				else
				{
					out << "{" << f.name;

					for (auto const & a : f.args)
					{
						out << ' ';
						visit(a);
					}

					out << '}';
				}
			}

			void operator() (application const & app)
			{
				out << '(';
				visit(app.func);
				out << ' ';
				visit(app.arg);
				out << ')';
			}

			void operator() (abstraction const & ab)
			{
				out << "(\\" << ab.vars[0];
				for (std::size_t i = 1; i < ab.vars.size(); ++i)
					out << ',' << ab.vars[i];
				out << " -> ";
				visit(ab.body);
				out << ')';
			}

			void operator() (product const & p)
			{
				if (p.empty())
					out << "unit";
				else
				{
					out << '(';
					out << p.front();
					for (std::size_t i = 1; i < p.size(); ++i)
						out << ',' << p[i];
					out << ')';
				}
			}
		};

	}

	std::ostream & operator << (std::ostream & os, expression const & e)
	{
		visitor{os}.visit(e);
		return os;
	}

}
