#include <typi/expression.hpp>

#include <optional>

namespace typi::expr_tree
{

	namespace
	{

		struct visitor : expression::const_visitor<expression, visitor>
		{
			std::map<variable, expression> m;

			visitor (std::map<variable, expression> m)
				: m{std::move(m)}
			{ }

			expression operator() (bool_constant b)
			{
				return b;
			}

			expression operator() (int_constant i)
			{
				return i;
			}

			expression operator() (variable v)
			{
				return m.count(v) > 0 ? m[v] : v;
			}

			expression operator() (foreign b)
			{
				return b;
			}

			expression operator() (abstraction ab)
			{
				std::set<variable> fv_all;

				for (auto const & p : m)
				{
					auto fv = free_vars(p.second);
					fv_all.insert(fv.begin(), fv.end());
				}

				std::vector<std::optional<expression>> temp(ab.vars.size());

				for (std::size_t i = 0; i < ab.vars.size(); ++i)
				{
					if (m.count(ab.vars[i]) > 0)
					{
						temp[i] = m[ab.vars[i]];
						m.erase(ab.vars[i]);
					}
					else
					{
						if (fv_all.count(ab.vars[i]) > 0)
						{
							auto new_var = *fv_all.rbegin() + "'";

							ab.body = substitute(ab.body, {{ab.vars[i], new_var}});
							ab.vars[i] = new_var;
						}
					}
				}

				auto result = abstraction{ ab.vars, visit(ab.body) };

				for (std::size_t i = 0; i < ab.vars.size(); ++i)
				{
					if (temp[i])
					m[ab.vars[i]] = *temp[i];
				}

				return result;
			}

			expression operator() (application const & app)
			{
				auto func = visit(app.func);
				auto arg = visit(app.arg);
				return application{ func, arg };
			}


			expression operator() (product const & p)
			{
				product res;
				for (auto const & t : p)
					res.push_back(visit(t));
				return res;
			}
		};

	}

	expression substitute (expression const & e, std::map<variable, expression> const & m)
	{
		return visitor{m}.visit(e);
	}

}
