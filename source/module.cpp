#include <typi/module.hpp>
#include <typi/parser.hpp>
#include <typi/core.hpp>

#include <fstream>
#include <algorithm>

#include <experimental/filesystem>
#include <dlfcn.h>

namespace typi
{

	static const char * import_path = TYPI_IMPORT_PATH;

	using namespace std::experimental::filesystem;

	void load_text_module (std::string const &, path const & module_path, context_t & context, module_cache_t & module_cache)
	{
		std::ifstream ifs(module_path);

		std::size_t line_index = 0;

		std::string line;
		while (std::getline(ifs, line))
		{
			++line_index;

			if (std::all_of(line.begin(), line.end(), [](char c){ return std::isspace(c); }))
				continue;

			if (line[0] == '#')
				continue;

			if (line.find("import") == 0)
			{
				load_module(line.substr(7), context, module_cache);
				continue;
			}

			expr_tree::variable name;

			auto assignment = line.find(":=");

			if (assignment == std::string::npos)
				throw std::runtime_error("unknown module statement at line " + std::to_string(line_index));

			name = parser{line.substr(0, assignment)}.parse_name();
			line = line.substr(assignment + 2);

			typi::parser p{line};

			auto expr = p.parse();

			auto s = substitute(expr, context);

			auto t = infer_type(s);

			context[name] = s;
		}
	}

	void load_binary_module (std::string const & module_name, path const & module_path, context_t & context, module_cache_t &)
	{
		auto fail_with = [&](std::string const & what)
		{
			throw std::runtime_error("failed to load binary module " + module_name + ": " + what);
		};

		void * module = dlopen(module_path.c_str(), RTLD_LAZY | RTLD_LOCAL);
		if (!module)
			fail_with(dlerror());

		auto entry = (typi::binary_entry_point) dlsym(module, "entry_point");

		if (!entry)
			fail_with("entry point missing");

		auto symbols = entry();

		for (auto const & p : symbols)
		{
			if (context.count(p.first) > 0)
				fail_with("symbol " + p.first + " already defined");
		}

		for (auto const & p : symbols)
		{
			context[p.first] = p.second;
		}
	}

	void load_module (std::string const & module_name, context_t & context, module_cache_t & module_cache)
	{
		if (module_cache.count(module_name) > 0)
			return;

		path binary_module_path { import_path + std::string{"/"} + module_name + ".so" };
		if (exists(binary_module_path))
		{
			load_binary_module(module_name, binary_module_path, context, module_cache);
			module_cache.insert(module_name);
			std::cout << "Loaded module " << module_name << std::endl;
			return;
		}

		path text_module_path { import_path + std::string{"/"} + module_name };
		if (exists(text_module_path))
		{
			load_text_module(module_name, text_module_path, context, module_cache);
			module_cache.insert(module_name);
			std::cout << "Loaded module " << module_name << std::endl;
			return;
		}

		throw std::runtime_error("failed to find module " + module_name);
	}


}
