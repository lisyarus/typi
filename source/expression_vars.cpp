#include <typi/expression.hpp>

namespace typi::expr_tree
{

	namespace
	{

		using var_set = std::set<expr_tree::variable>;

		struct visitor : expression::const_visitor<var_set, visitor>
		{
			var_set operator() (expr_tree::bool_constant)
			{
				return {};
			}

			var_set operator() (expr_tree::int_constant)
			{
				return {};
			}

			var_set operator() (expr_tree::variable const & v)
			{
				return {v};
			}

			var_set operator() (expr_tree::foreign const & f)
			{
				var_set res;

				for (auto const & e : f.args)
				{
					auto s = visit(e);
					res.insert(s.begin(), s.end());
				}

				return res;
			}

			var_set operator() (expr_tree::application const & app)
			{
				auto s1 = visit(app.func);
				auto s2 = visit(app.arg);

				s1.insert(s2.begin(), s2.end());
				return s1;
			}

			var_set operator() (expr_tree::abstraction const & ab)
			{
				auto s = visit(ab.body);

				for (auto v : ab.vars)
					s.erase(v);

				return s;
			}

			var_set operator() (expr_tree::product const & p)
			{
				var_set res;

				for (auto const & e : p)
				{
					auto s = visit(e);
					res.insert(s.begin(), s.end());
				}

				return res;
			}
		};
	}

	std::set<expr_tree::variable> free_vars (expr_tree::expression const & e)
	{
		return visitor{}.visit(e);
	}

}
