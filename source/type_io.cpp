#include <typi/type.hpp>

namespace typi::type_tree
{

	namespace
	{

		struct visitor : type::const_visitor<void, visitor>
		{
			std::ostream & out;

			visitor (std::ostream & out)
				: out{out}
			{ }

			void operator() (variable const & v)
			{
				out << v;
			}

			void operator() (bool_type)
			{
				out << "bool";
			}

			void operator() (int_type)
			{
				out << "int";
			}

			void operator() (function const & f)
			{
				out << '(';
				visit(f.arg);
				out << " -> ";
				visit(f.ret);
				out << ')';
			}

			void operator() (product const & p)
			{
				if (p.empty())
					out << "unit";
				else
				{
					out << '(' << p.front();
					for (std::size_t i = 1; i < p.size(); ++i)
					{
						out << " * " << p[i];
					}
					out << ')';
				}
			}
		};

	}

	std::ostream & operator << (std::ostream & os, type const & t)
	{
		visitor{os}.visit(t);
		return os;
	}

}
