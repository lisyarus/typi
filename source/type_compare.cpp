#include <typi/type.hpp>

namespace typi::type_tree
{

	namespace
	{

		struct visitor : type::const_binary_visitor<bool, visitor>
		{
			bool operator() (variable const & v1, variable const & v2)
			{
				return v1 == v2;
			}

			bool operator() (bool_type, bool_type)
			{
				return true;
			}

			bool operator() (int_type, int_type)
			{
				return true;
			}

			bool operator() (function const & f1, function const & f2)
			{
				return visit(f1.arg, f2.arg) && visit(f1.ret, f2.ret);
			}

			bool operator() (type_tree::product const & p1, type_tree::product const & p2)
			{
				if (p1.size() != p2.size())
					return false;

				for (std::size_t i = 0; i < p1.size(); ++i)
					if (!visit(p1[i], p2[i]))
						return false;

				return true;
			}

			template <typename T1, typename T2>
			bool operator() (T1 const &, T2 const &)
			{
				return false;
			}
		};

	}

	bool operator == (type const & t1, type const & t2)
	{
		return visitor{}.visit(t1, t2);
	}

}
