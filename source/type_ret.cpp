#include <typi/type.hpp>

namespace typi::type_tree
{

	type ret_type (type const & t)
	{
		if (auto p = std::get_if<function>(t.ptr.get()))
		{
			return p->ret;
		}

		throw std::runtime_error("function type expected");
	}

}
