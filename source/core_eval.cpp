#include <typi/core.hpp>

namespace typi
{

	namespace
	{

		// performs single beta-reduction
		struct visitor : expr_tree::expression::const_visitor<expr_tree::expression, visitor>
		{
			expr_tree::expression operator() (expr_tree::bool_constant b)
			{
				return b;
			}

			expr_tree::expression operator() (expr_tree::int_constant i)
			{
				return i;
			}

			expr_tree::expression operator() (expr_tree::variable v)
			{
				return v;
			}

			expr_tree::expression operator() (expr_tree::abstraction ab)
			{
				return ab;
			}

			expr_tree::expression operator() (expr_tree::foreign const & f)
			{
				if (f.args_left == 0)
				{
					std::vector<expr_tree::expression> args;
					for (auto const & a : f.args)
						args.push_back(a);

					auto r = f.func_ptr(args);
					if (r)
						return r;
				}

				return f;
			}

			expr_tree::expression operator() (expr_tree::application const & app)
			{
				if (auto p = std::get_if<expr_tree::abstraction>(app.func.ptr.get()))
				{
					if (p->vars.size() == 1)
						return expr_tree::substitute(p->body, {{p->vars[0], app.arg}});
					else if (auto a = std::get_if<expr_tree::product>(app.arg.ptr.get()))
					{
						if (a->size() == p->vars.size())
						{
							std::map<expr_tree::variable, expr_tree::expression> m;

							for (std::size_t i = 0; i < p->vars.size(); ++i)
							{
								m[p->vars[i]] = (*a)[i];
							}

							return expr_tree::substitute(p->body, m);
						}
					}
				}

				if (auto p = std::get_if<expr_tree::foreign>(app.func.ptr.get()))
				{
					if (p->args_left > 0)
					{
						std::vector<expr_tree::expression> args = p->args;
						args.push_back(app.arg);
						return expr_tree::foreign{args, p->args_left - 1, ret_type(p->type), p->func_ptr, p->name};
					}
				}

				auto func = visit(app.func);
				if (func != app.func)
					return expr_tree::application{ func, app.arg };

				auto arg = visit(app.arg);
				if (arg != app.arg)
					return expr_tree::application{ app.func, arg };

				return app;
			}

			expr_tree::expression operator() (expr_tree::product const & p)
			{
				expr_tree::product res;
				for (auto const & e : p)
					res.push_back(visit(e));
				return res;
			}
		};

	}

	expr_tree::expression eval (expr_tree::expression const & e, std::size_t max_iterations)
	{
		visitor v;
		auto current = e;
		while (max_iterations --> 0)
		{
			auto reduced = v.visit(current);
			if (reduced == current)
				return reduced;
			current = reduced;
		}

		throw std::runtime_error("maximum number of iterations exceeded");
	}

}
