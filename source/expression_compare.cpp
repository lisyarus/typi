#include <typi/expression.hpp>
#include <typi/utility.hpp>

namespace typi::expr_tree
{

	namespace
	{

		struct visitor : expression::const_binary_visitor<bool, visitor>
		{
			bool operator() (variable const & v1, variable const & v2)
			{
				return v1 == v2;
			}

			bool operator() (bool_constant const & b1, bool_constant const & b2)
			{
				return b1 == b2;
			}

			bool operator() (int_constant const & i1, int_constant const & i2)
			{
				return i1 == i2;
			}

			bool operator() (foreign const & f1, foreign const & f2)
			{
				if (f1.args.size() != f2.args.size())
					return false;

				if (f1.args_left != f2.args_left)
					return false;

				if (f1.func_ptr != f2.func_ptr)
					return false;

				for (std::size_t i = 0; i < f1.args.size(); ++i)
					if (!visit(f1.args[i], f2.args[i]))
						return false;

				return true;
			}

			bool operator() (application const & app1, application const & app2)
			{
				return visit(app1.func, app2.func) && visit(app1.arg, app2.arg);
			}

			bool operator() (abstraction const & ab1, abstraction const & ab2)
			{
				if (ab1.vars.size() != ab2.vars.size())
					return false;

				// find all free vars in ab1 & ab2
				// rename bound vars to something not in there

				auto fv1 = free_vars(ab1);
				auto fv2 = free_vars(ab1);

				fv1.insert(fv2.begin(), fv2.end());

				expr_tree::variable cur_var = "a";
				if (!fv1.empty())
					cur_var = next_var(*fv1.rbegin());

				std::map<expr_tree::variable, expr_tree::expression> m1;
				std::map<expr_tree::variable, expr_tree::expression> m2;

				for (std::size_t i = 0; i < ab1.vars.size(); ++i)
				{
					m1[ab1.vars[i]] = cur_var;
					m2[ab2.vars[i]] = cur_var;

					cur_var = next_var(cur_var);
				}

				auto b1 = substitute(ab1.body, m1);
				auto b2 = substitute(ab2.body, m2);

				return visit(b1, b2);
			}

			bool operator() (product const & p1, product const & p2)
			{
				if (p1.size() != p2.size())
					return false;

				for (std::size_t i = 0; i < p1.size(); ++i)
					if (!visit(p1[i], p2[i]))
						return false;

				return true;
			}

			template <typename A, typename B>
			bool operator() (A const &, B const &)
			{
				return false;
			}
		};

	}

	bool operator == (expression const & e1, expression const & e2)
	{
		return visitor{}.visit(e1, e2);
	}

}
