#pragma once

#include <typi/tree.hpp>

#include <string>
#include <vector>
#include <iostream>
#include <set>
#include <map>

namespace typi::type_tree
{

	using variable = std::string;

	struct bool_type { };
	struct int_type { };

	namespace detail
	{

		template <typename A>
		struct function
		{
			A arg;
			A ret;
		};

	}

	using type = tree<
		variable,
		bool_type,
		int_type,
		detail::function<self>,
		std::vector<self>
	>;

	using product = std::vector<type>;

	using function = detail::function<type>;

	bool operator == (type const & t1, type const & t2);

	inline bool operator != (type const & t1, type const & t2)
	{
		return !(t1 == t2);
	}

	std::ostream & operator << (std::ostream & os, type const & t);

	type substitute (type const & t, std::map<variable, type> const & m);

	// (a -> b)  =>  b
	type ret_type (type const & t);

	std::set<variable> free_vars (type const & t);

}


namespace typi
{

	using type_tree::operator <<;
	using type_tree::operator ==;
	using type_tree::operator !=;
	using type_tree::free_vars;
	using type_tree::ret_type;
	using type_tree::substitute;

}
