#pragma once

#include <typi/expression.hpp>
#include <typi/type.hpp>

namespace typi
{

	using unifier_t = std::map<type_tree::variable, type_tree::type>;
	using equation_t = std::pair<type_tree::type, type_tree::type>;

	unifier_t unify (std::vector<equation_t> equations);
	unifier_t unroll (unifier_t u);

	using type_map_t = std::map<expr_tree::variable, type_tree::type>;

	std::pair<type_tree::type, type_map_t> infer_type (expr_tree::expression const & e);

	expr_tree::expression eval (expr_tree::expression const & e, std::size_t max_iterations = 1024);

}
