#pragma once

#include <typi/tree.hpp>
#include <typi/type.hpp>

#include <string>
#include <vector>
#include <iostream>
#include <set>
#include <map>
#include <cstdint>

namespace typi::expr_tree
{

	using variable = std::string;

	using bool_constant = bool;
	using int_constant = std::int64_t;

	using pattern = std::vector<variable>;

	namespace detail
	{

		template <typename A>
		struct application
		{
			A func;
			A arg;
		};

		template <typename A>
		struct abstraction
		{
			pattern vars;
			A body;
		};

		template <typename A>
		struct foreign
		{
			std::vector<A> args;
			std::size_t args_left;

			type_tree::type type;

			A (*func_ptr)(std::vector<A> const &);

			std::string name;
		};

	}

	using expression = tree<
		variable,
		bool_constant,
		int_constant,
		detail::foreign<self>,
		detail::application<self>,
		detail::abstraction<self>,
		std::vector<self>
	>;

	using foreign = detail::foreign<expression>;
	using application = detail::application<expression>;
	using abstraction = detail::abstraction<expression>;
	using product = std::vector<expression>;

	std::ostream & operator << (std::ostream & os, expression const & e);

	bool operator == (expression const & e1, expression const & e2);

	inline bool operator != (expression const & e1, expression const & e2)
	{
		return !(e1 == e2);
	}

	std::set<variable> free_vars (expression const & e);

	expression substitute (expression const & e, std::map<variable, expression> const & m);

}

namespace typi
{

	using expr_tree::operator <<;
	using expr_tree::operator ==;
	using expr_tree::operator !=;
	using expr_tree::free_vars;
	using expr_tree::substitute;

}
