#pragma once

#include <typi/expression.hpp>

namespace typi
{

	using context_t = std::map<expr_tree::variable, expr_tree::expression>;
	using module_cache_t = std::set<std::string>;

	void load_module (std::string const & module_name, context_t & context, module_cache_t & module_cache);

	using binary_entry_point = context_t (*)();

}

#define TYPI_MODULE_ENTRY_POINT extern "C" __attribute__((visibility("default"))) typi::context_t entry_point ( )
