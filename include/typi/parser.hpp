#pragma once

#include <typi/expression.hpp>

namespace typi
{

	expr_tree::expression fill_keywords (expr_tree::expression const & e);

	struct parser
	{
		parser (std::string_view str)
			: begin(str.begin())
			, end(str.end())
		{ }

		expr_tree::expression parse ( );

		expr_tree::expression   parse_application ( );
		expr_tree::expression   parse_term ( );
		expr_tree::expression   parse_literal ( );
		expr_tree::variable     parse_name ( );
		expr_tree::int_constant parse_int ( );

		char peek ( ) const { check_eof(); return *begin; }
		bool eof ( ) const { return begin == end; }

	private:
		char const * begin;
		char const * end;
		char next ( ) { check_eof(); return *begin++; }

		void check_eof ( ) const;
		void skip_space ( );
		void expect (char c);
	};

}
