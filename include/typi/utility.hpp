#pragma once

#include <stdexcept>

#define TYPI_NOT_IMPLEMENTED throw std::runtime_error(__FILE__ ":" + std::to_string(__LINE__) + " not implemented")

namespace typi
{

	inline std::string next_var (std::string const & var)
	{
		std::string v = var;
		v[0]++;
		return v;
	}

}
