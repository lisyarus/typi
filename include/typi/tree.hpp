#pragma once

#include <variant>
#include <memory>
#include <type_traits>

namespace typi
{

	struct self { };

	namespace detail
	{

		template <typename Option, typename Tree>
		struct wrap_type
		{
			using type = Option;
		};

		template <typename Option, typename Tree>
		using wrap_type_t = typename wrap_type<Option, Tree>::type;

		template <template <typename> typename Option, typename Tree>
		struct wrap_type<Option<self>, Tree>
		{
			using type = Option<Tree>;
		};

		template <typename T, typename ... Ts>
		struct one_of;

		template <typename T, typename ... Ts>
		constexpr bool one_of_v = one_of<T, Ts...>::value;

		template <typename T>
		struct one_of<T>
		{
			static constexpr bool value = false;
		};

		template <typename T, typename First, typename ... Ts>
		struct one_of<T, First, Ts...>
		{
			static constexpr bool value = one_of<T, Ts...>::value;
		};

		template <typename T, typename ... Ts>
		struct one_of<T, T, Ts...>
		{
			static constexpr bool value = true;
		};

		template <typename T, typename Variant>
		struct one_of_variant;

		template <typename T, typename Variant>
		constexpr bool one_of_variant_v = one_of_variant<T, Variant>::value;

		template <typename T, typename ... Ts>
		struct one_of_variant<T, std::variant<Ts...>>
		{
			static constexpr bool value = one_of<T, Ts...>::value;
		};

	}

	template <typename ... Ts>
	struct tree
	{
		using variant_type = std::variant<detail::wrap_type_t<Ts, tree>...>;
		std::shared_ptr<variant_type> ptr;

		tree ( ) = default;
		tree (tree const &) = default;
		tree (tree &&) = default;
		tree & operator = (tree const &) = default;
		tree & operator = (tree &&) = default;

		explicit operator bool ( ) const
		{
			return static_cast<bool>(ptr);
		}

		template <typename X, typename = std::enable_if_t<detail::one_of_variant_v<std::decay_t<X>, variant_type>>>
		tree (X && x)
			: ptr{ std::make_shared<variant_type>(std::forward<X>(x)) }
		{ }

		template <typename R, typename Derived>
		struct visitor
		{
			R visit (tree<Ts...> & t)
			{
				return std::visit(static_cast<Derived&>(*this), *(t.ptr));
			}
		};

		template <typename R, typename Derived>
		struct const_visitor
		{
			R visit (tree<Ts...> const & t)
			{
				return std::visit(static_cast<Derived&>(*this), *(t.ptr));
			}
		};

		template <typename R, typename Derived>
		struct binary_visitor
		{
			R visit (tree<Ts...> & t1, tree<Ts...> & t2)
			{
				return std::visit(static_cast<Derived&>(*this), *(t1.ptr), *(t2.ptr));
			}
		};

		template <typename R, typename Derived>
		struct const_binary_visitor
		{
			R visit (tree<Ts...> const & t1, tree<Ts...> const & t2)
			{
				return std::visit(static_cast<Derived&>(*this), *(t1.ptr), *(t2.ptr));
			}
		};
	};

}
