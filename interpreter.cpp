#include <typi/expression.hpp>
#include <typi/type.hpp>
#include <typi/parser.hpp>
#include <typi/core.hpp>
#include <typi/module.hpp>
#include <typi/utility.hpp>

#include <sstream>
#include <algorithm>
#include <map>
#include <vector>
#include <set>
#include <chrono>
#include <optional>

int main ( )
{
	using namespace typi;

	module_cache_t module_cache;
	context_t context;

	load_module("std", context, module_cache);

	std::string line;
	while (std::cout << ">> ", std::getline(std::cin, line))
	{
		try
		{
			if (std::all_of(line.begin(), line.end(), [](char c){ return std::isspace(c); }))
				continue;

			if (line[0] == '#')
				continue;

			if (line.find("import") == 0)
			{
				load_module(line.substr(7), context, module_cache);
				continue;
			}

			if (line == "?")
			{
				for (auto const & p : context)
				{
					std::cout << p.first << " : " << infer_type(p.second).first << std::endl;
				}

				continue;
			}

			std::optional<expr_tree::variable> name;

			auto assignment = line.find(":=");

			if (assignment != std::string::npos)
			{
				name = parser{line.substr(0, assignment)}.parse_name();
				line = line.substr(assignment + 2);
			}

			typi::parser p{line};

			using clock = std::chrono::high_resolution_clock;

			auto const start_time = clock::now();

			auto expr = p.parse();

			if (!p.eof())
				throw std::runtime_error("unexpected symbol: " + std::string{p.peek()});

			auto s = substitute(expr, context);

			auto t = infer_type(s);

			// Rename type variables so that they look a bit more friendly
			{
				auto t_fv = free_vars(t.first);

				for (auto const & p : t.second)
				{
					auto fv = free_vars(p.second);
					t_fv.insert(fv.begin(), fv.end());
				}

				std::map<type_tree::variable, type_tree::type> var_map;
				type_tree::variable cur_v = "a";

				for (auto const & v : t_fv)
				{
					var_map[v] = cur_v;
					cur_v = next_var(cur_v);
				}

				auto new_type = substitute(t.first, var_map);

				std::map<expr_tree::variable, type_tree::type> new_type_map;

				for (auto const & p : t.second)
					new_type_map[p.first] = substitute(p.second, var_map);

				t = {new_type, new_type_map};
			}

			// do not evaluate on assignment
			if (name)
				context[*name] = s;

			auto v = eval(s);

			auto const end_time = clock::now();

			std::cout << v << " : " << t.first << std::endl;
			for (auto const & p : t.second)
			{
				if (p.first != v)
					std::cout << p.first << " : " << p.second << std::endl;
			}
			std::cout << std::chrono::duration_cast<std::chrono::duration<double>>(end_time - start_time).count() << " seconds" << std::endl;
		}
		catch (std::exception const & e)
		{
			std::cout << "Error: " << e.what() << std::endl;
		}

		std::cout << std::endl;
	}
}
