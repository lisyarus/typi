#include <typi/module.hpp>
#include <typi/core.hpp>

using namespace typi;

namespace module
{

	auto const binary_op_type = [](type_tree::type ret){
		return type_tree::function{ type_tree::int_type{}, type_tree::function{ type_tree::int_type{}, ret }};
	};

#define BINARY_OP_IMPL(name, op, ret) \
	expr_tree::expression name##_impl (std::vector<expr_tree::expression> const & args) \
	{ \
		auto v1 = eval(args[0]); \
		auto v2 = eval(args[1]); \
		auto a1 = std::get_if<expr_tree::int_constant>(v1.ptr.get()); \
		auto a2 = std::get_if<expr_tree::int_constant>(v2.ptr.get()); \
		if (a1 && a2) \
		{ \
			return (*a1) op (*a2); \
		} \
		\
		return {}; \
	} \
	auto const name = expr_tree::foreign{ {}, 2, binary_op_type(ret), name##_impl, #op };

	BINARY_OP_IMPL(add, +, type_tree::int_type{})
	BINARY_OP_IMPL(sub, -, type_tree::int_type{})
	BINARY_OP_IMPL(mult, *, type_tree::int_type{})
	BINARY_OP_IMPL(div, /, type_tree::int_type{})

	BINARY_OP_IMPL(lt, <, type_tree::bool_type{})

}

TYPI_MODULE_ENTRY_POINT
{
	using namespace module;

	return {
		{"+", add},
		{"-", sub},
		{"*", mult},
		{"/", module::div},
		{"<", lt},
	};
}
