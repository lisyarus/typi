#include <typi/module.hpp>
#include <typi/core.hpp>

using namespace typi;
using namespace std::literals;

namespace module
{

	expr_tree::expression fix_impl (std::vector<expr_tree::expression> const & args);

	auto const fix_type = type_tree::function{ type_tree::function{ "a"s, "a"s }, "a"s };
	auto const fix = expr_tree::foreign{ {}, 1, fix_type, fix_impl, "fix" };

	expr_tree::expression fix_impl (std::vector<expr_tree::expression> const & args)
	{
		return expr_tree::application{ args[0], expr_tree::application{ fix, args[0] } };
	}

	expr_tree::expression eq_impl (std::vector<expr_tree::expression> const & args)
	{
		return eval(args[0]) == eval(args[1]);
	}

	auto const eq_type = type_tree::function{ "a"s, type_tree::function{ "a"s, type_tree::bool_type{} }};
	auto const eq = expr_tree::foreign{ {}, 2, eq_type, eq_impl, "==" };

	expr_tree::expression if_true ( )
	{
		return expr_tree::abstraction{ {"x"}, expr_tree::abstraction{ {"y"}, expr_tree::variable{ "x" } } };
	}

	expr_tree::expression if_false ( )
	{
		return expr_tree::abstraction{ {"x"}, expr_tree::abstraction{ {"y"}, expr_tree::variable{ "y" } } };
	}

	auto const if_type = type_tree::function{
			type_tree::bool_type{},
			type_tree::function{
				"a"s,
				type_tree::function{
					"a"s,
					"a"s
				}
			}
		};

	expr_tree::expression if_impl (std::vector<expr_tree::expression> const & args)
	{
		auto cond = eval(args[0]);
		if (auto p = std::get_if<expr_tree::bool_constant>(cond.ptr.get()))
		{
			if (*p)
				return if_true();
			else
				return if_false();
		}

		return {};
	}

	auto const if_ = expr_tree::foreign{ {}, 1, if_type, if_impl, "if" };

	expr_tree::expression undefined_impl (std::vector<expr_tree::expression> const &)
	{
		throw std::runtime_error("undefined");
	}

	auto const undefined = expr_tree::foreign{ {}, 0, "a"s, undefined_impl, "undefined" };

	expr_tree::expression seq_impl (std::vector<expr_tree::expression> const & args)
	{
		return eval(args[0]);
	}

	auto const seq = expr_tree::foreign{ {}, 1, type_tree::function{ "a"s, "a"s }, seq_impl, "seq" };

	expr_tree::expression print_impl (std::vector<expr_tree::expression> const & args)
	{
		std::cout << "DEBUG: " << args[0] << std::endl;
		return args[0];
	}

	auto const print = expr_tree::foreign{ {}, 1, type_tree::function{ "a"s, "a"s }, print_impl, "print" };

}

TYPI_MODULE_ENTRY_POINT
{
	using namespace module;

	return {
		{fix.name, fix},
		{if_.name, if_},
		{eq.name, eq},
		{undefined.name, undefined},
		{seq.name, seq},
		{print.name, print},
	};
}
